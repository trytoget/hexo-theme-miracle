<h1 align="center">Hexo Theme Miracle</h1>
<p align="center">A simple single-column theme for Hexo.</p>
<p align="center">
    <a href="https://github.com/hifun-team/hexo-theme-miracle/releases/"><img src="https://badgen.net/github/release/hifun-team/hexo-theme-miracle" alt="GitHub Release"></a>
    <a href="https://github.com/hifun-team/hexo-theme-miracle/stargazers"><img src="https://badgen.net/github/stars/hifun-team/hexo-theme-miracle" alt="GitHub Stars"></a>
    <a href="https://github.com/hifun-team/hexo-theme-miracle/network/members"><img src="https://badgen.net/github/forks/hifun-team/hexo-theme-miracle" alt="GitHub Forks"></a>
    <a href="https://github.com/hifun-team/hexo-theme-miracle/watchers"><img src="https://badgen.net/github/watchers/hifun-team/hexo-theme-miracle" alt="GitHub Watchers"></a>
    <a href="https://github.com/hifun-team/hexo-theme-miracle/blob/master/LICENSE"><img src="https://badgen.net/github/license/hifun-team/hexo-theme-miracle" alt="MIT License"></a>
</p>

## Features

- [x] Code highlight
- [x] Built-in multiple languages
- [x] Various comment plugins
- [x] Custom static resource CDN
- [x] Site search
- [x] Footer record information
- [x] Web statistics
- [x] Support footnote syntax
- [x] Dark mode
- [x] Responsive / adaptive design
- [x] Article TOC
- [x] Optimized reading experience
- [x] Mermaid

## Links

- [Theme Demo](https://miracle-demo.now.sh)
- [Theme Document](https://miracle-docs.now.sh)

## Install themes

Please check the document: [Download and Install](https://miracle-docs.now.sh/?p=en#download-and-install)

## Update theme

Please check the document: [Update Theme](https://miracle-docs.now.sh/?p=en#update-theme)

## Participate in contribution

### Contribution Guidelines

1. Fork the warehouse to your GitHub and clone it locally.
2. Create a new branch and develop on the new branch.
3. Keep the branch consistent with the remote `master` branch (via fetch and rebase operations).
4. Submit the changes locally (note that the commit log is concise and standardized, and it is recommended to add Emoji).
5. Push the submission to the Fork warehouse.
6. Create a `Pull request` to the `master` branch of `hifun-team/hexo-theme-miracle`.

### Contributors

<a href="https://github.com/oCoke" title="oCoke"><img src="https://avatars0.githubusercontent.com/u/71591824?s=460&u=4e1a04eadb3b23add8f5c9ba14e21b00219142f7&v=4" alt= "oCoke" height="68" width="68" style="max-width:100%;"></a>


## Reference

This project reference some open source theme projects, thanks very much!

- [fluid-dev/hexo-theme-fluid](https://github.com/fluid-dev/hexo-theme-fluid)
- [XPoet/hexo-theme-keep](https://github.com/XPoet/hexo-theme-keep)